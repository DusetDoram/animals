package ru.fayzulloev.animals;

/**
 * Created by DuaetDoram on 16.10.2014.
 */
public class Cat implements Animals {
    @Override
    public String getName() {
        return "Кошка";
    }

    @Override
    public String gatVoice() {
        return "Мяу-Мяу";
    }
}

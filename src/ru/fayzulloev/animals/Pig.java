package ru.celovalnicov.animals;

/**
 * Created by ог on 16.10.2014.
 */
public class Pig implements Animals {
    @Override
    public String getName() {
        return "Свинья";
    }

    @Override
    public String getVoice() {
        return "Хрю-хрю";
    }
}

package ru.celovalnicov.animals;

/**
 * Created by ог on 16.10.2014.
 */
public class Chicken implements Animals {
    @Override
    public String getName() {
        return "Циплёнок";
    }

    @Override
    public String getVoice() {
        return "Пи-пи";
    }
}

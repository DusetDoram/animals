package ru.fayzulloev.animals;

/**
 * Created by DuaetDoram on 16.10.2014.
 */
public class Dog implements Animals {
    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String gatVoice() {
        return "гав-гав";
    }
}

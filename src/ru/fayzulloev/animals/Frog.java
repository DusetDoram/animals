package ru.fayzulloev.animals;

/**
 * Created by DuaetDoram on 21.10.2014.
 */
public class Frog implements Animals {

    @Override
    public String getName () {
        return "Лягушка";
    }

    @Override
    public String gatVoice () {
        return "Квак-Квак";
    }
}

